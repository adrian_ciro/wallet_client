# Wallet client made with Spring Boot Application 


## How to execute it

1 Build this project
```
gradle build
```

2 Run the application

2.1 For automatic mode
```
gradle run
```

2.2 For manual mode
```
java -jar build/libs/clientWallet-0.1.0.jar "manual" Application
```
