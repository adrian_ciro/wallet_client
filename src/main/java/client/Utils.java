package client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by adrian on 11/09/17.
 */
public class Utils {

    private static String COMMAND_REGEX = "(\\w+\\s\\d+\\s\\d+)(\\.\\d+)?";
    public static String URL = "http://localhost:8080/api/updateBalance";
    public static boolean enableAutomaticRequest = true;

    public static boolean isValidCommand(String command){
        Matcher matcher = Pattern.compile(COMMAND_REGEX).matcher(command);
        return matcher.matches();
    }
}
