package client.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

    private Long transactionId;
    private String username;
    private Double value;


    public Request(String username, Long transactionId, Double value) {
        this.username = username;
        this.transactionId = transactionId;
        this.value = value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Request{" +
                "username='" + username + '\'' +
                ", transactionId=" + transactionId +
                ", value=" + value +
                '}';
    }
}
