package client.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    private Long transactionId;
    private String error;
    private Long balanceVersion;
    private Double balanceChange;
    private Double newBalance;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getBalanceVersion() {
        return balanceVersion;
    }

    public void setBalanceVersion(Long balanceVersion) {
        this.balanceVersion = balanceVersion;
    }

    public Double getBalanceChange() {
        return balanceChange;
    }

    public void setBalanceChange(Double balanceChange) {
        this.balanceChange = balanceChange;
    }

    public Double getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(Double newBalance) {
        this.newBalance = newBalance;
    }

    @Override
    public String toString() {
        return "Response{" +
                "error='" + error + '\'' +
                ", transactionId=" + transactionId +
                ", balanceVersion=" + balanceVersion +
                ", balanceChange=" + balanceChange +
                ", newBalance=" + newBalance +
                '}';
    }
}
