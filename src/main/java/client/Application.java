package client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Scanner;

@SpringBootApplication
@EnableScheduling
public class Application {

	private static final String EXECUTION_MODE = "manual";
	public static final String INITIAL_MSG = "\n\nPlease \"ENTER\" the transaction command to send to the server e.g 'username transactionid balanceChange'";
	public static final String CORRECTION_MSG = "Please enter correct format in the console e.g 'username transactionid balanceChange'";

	public static void main(String args[]) {

		if(args.length>0 && EXECUTION_MODE.equalsIgnoreCase(args[0])){

			Utils.enableAutomaticRequest = false;
			System.out.println(INITIAL_MSG);
			Controller controller = new Controller();

			while (true) {
				String command;
				Scanner scanner = new Scanner(System.in);
				command = scanner.nextLine();
				if (StringUtils.isEmpty(command) || !Utils.isValidCommand(command)) {
					System.out.println(CORRECTION_MSG);
				} else {
					controller.sendRequest(command);
					System.out.println(INITIAL_MSG);
				}
			}

		}
		else {
			SpringApplication.run(Application.class);
		}
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder().build();
	}

}