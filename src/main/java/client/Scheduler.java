package client;

import client.domain.Request;
import client.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static client.Utils.URL;

/**
 * Class responsible for schedule the requests. Those are executed every 10000 ms
 */
@Component
public class Scheduler {

    private Long transactionId = 0L;

    private static final List<String> USERNAMES = Arrays.asList("adrian", "david", "viktoria");
    private static final List<Double> BALANCES = Arrays.asList(10.0, 50.0, 100.0);
    private Random rand;

    private static final Logger log = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    private RestTemplate restTemplate;

    public Scheduler() {
        rand = new Random();
    }

    @Scheduled(fixedDelay = 10000)
    private void runUpdates() {
        if(Utils.enableAutomaticRequest){
            Request request =  createRequest();
            log.info(request.toString());
            Response response = restTemplate.postForObject(URL, request, Response.class);
            log.info(response.toString());
        }

    }

    private Request createRequest(){
        return new Request(USERNAMES.get(rand.nextInt(3)), transactionId++, BALANCES.get(rand.nextInt(3)));
    }
}
