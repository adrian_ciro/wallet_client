package client;

import client.domain.Request;
import client.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import static client.Utils.URL;

/**
 * Created by adrian on 11/09/17.
 */
public class Controller {

    private static final Logger log = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    private RestTemplate restTemplate;

    public void sendRequest(String command){
        if(restTemplate == null){
            restTemplate = new RestTemplateBuilder().build();
        }
        Response response = restTemplate.postForObject(URL, createRequest(command), Response.class);
        log.info(response.toString());
    }

    public Request createRequest(String command){
        String[] commands = command.split(" ");
        return new Request(commands[0], Long.valueOf(commands[1]), Double.valueOf(commands[2]));
    }

}
